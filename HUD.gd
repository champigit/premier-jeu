extends CanvasLayer

signal start_game

func show_message(text):
	$Message.text = text
	$Message.show()
	$MessageTimer.start()

func show_game_over():
	show_message("Game Over")
	# On attend que le MessageTimer soit terminé
	yield($MessageTimer, "timeout")
	
	$Message.text = "Attention aux bébettes !"
	$Message.show()
	# On crée un timer One shot et on attend qu'il termine. ça évite de créer un enfant timer juste pour une pause
	yield(get_tree().create_timer(1), "timeout")
	$StartButton.show()

func update_score(score):
	$ScoreLabel.text = str(score)

func _on_StartButton_pressed():
	$StartButton.hide()
	emit_signal("start_game")

func _on_MessageTimer_timeout():
	$Message.hide()
