extends Node

export (PackedScene) var Mob
var score

func _ready():
	randomize()

func game_over():
	$ScoreTimer.stop()
	$MobTimer.stop()
	$HUD.show_game_over()
	get_tree().call_group("mobs", "queue_free")
	$Music.stop()
	$DeathSound.play()

func new_game():
	score = 0
	$Player.start($StartPosition.position)
	$StartTimer.start()
	$HUD.update_score(score)
	$HUD.show_message("Attention...")
	$Music.play()

func _on_StartTimer_timeout():
	$MobTimer.start()
	$ScoreTimer.start()

func _on_ScoreTimer_timeout():
	score += 1
	$HUD.update_score(score)

func _on_MobTimer_timeout():
	# On choisit une position aléatoire sur le Path2D
	$MobPath/MobSpawnLocation.offset = randi()
	
	# On crée une instance Mob et on l'ajoute à la scène
	var mob = Mob.instance()
	add_child(mob)
	
	#On paramètre la direction du mob perpendiculaire à la direction du chemin
	var direction  = $MobPath/MobSpawnLocation.rotation + PI / 2
	
	# On positionne le mob n'importe où
	mob.position = $MobPath/MobSpawnLocation.position
	
	# On ajoute un peu d'aléatoire dans la direction
	direction += rand_range(-PI / 4, PI / 4)
	mob.rotation = direction
	
	# On paramètre la vélocité (vitesse et direction)
	mob.linear_velocity = Vector2(rand_range(mob.min_speed , mob.max_speed), 0)
	mob.linear_velocity = mob.linear_velocity.rotated(direction)

