extends Area2D

signal hit

export var speed = 400	# Vitesse du joueur
var screen_size	# Taille de la fenêtre de jeu


func _ready():
	screen_size = get_viewport_rect().size
	hide()

func _process(delta):
	var velocity = Vector2()	# Vecteur de mouvement du joueur
	
	# Test des touches pressées
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
	if Input.is_action_pressed("ui_up"):
		velocity.y -= 1
	
	# On fait bouger le sprite
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed 	# Si plusieurs touches sont pressées on fait en sorte que la vitesse ne soit pas sqrt(2)... Pythagore quoi...
		$AnimatedSprite.play()
	else:
		$AnimatedSprite.stop()
	
	# On fait bouger le joueur
	position += velocity * delta
	position.x = clamp(position.x, 0 ,screen_size.x)
	position.y = clamp(position.y, 0 ,screen_size.y)
	
	# On change les animations
	if velocity.x !=0:
		$AnimatedSprite.animation = "walk"
		$AnimatedSprite.flip_v = false
		$AnimatedSprite.flip_h = velocity.x < 0
	elif velocity.y !=0:
		$AnimatedSprite.animation = "up"
		$AnimatedSprite.flip_v = velocity.y > 0


func _on_Player_body_entered(body):
	hide()	# pouf le joueur disparait s'il est touché
	emit_signal("hit")
	$CollisionShape2D.set_deferred("disabled", true)	# On désactive la collision pour éviter qu'elle ne se lance plusieurs fois, mais seulement lorsque le traitement des collisions est terminé

func start(pos):
	position = pos
	show()
	$CollisionShape2D.disabled = false
